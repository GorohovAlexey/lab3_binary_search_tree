#pragma once
#include <iostream>
#include <queue>
#include <stack>

template <class T>
class BinarySearchTree
{
private:
	template <class T>
	struct Node
	{
		T key_;
		Node<T> *left_;
		Node<T> *right_;
		Node<T> *p_; 
		Node(T key, Node *left = nullptr, Node *right = nullptr, Node *p = nullptr) :
			key_(key), left_(left), right_(right), p_(p)
		{ }
	};
	Node<T>* iterativeSearchNode(const T& key) const;
	void printNode(Node<T>* node, std::ostream& out) const;
	int getCount(const Node<T>* node) const;
	int getHeight(const Node<T>* node) const;
	void inorderWalk(Node<T>* node) const;
	void swap(BinarySearchTree &other) noexcept;

	Node<T> *root_;

public:
	BinarySearchTree();
	BinarySearchTree(const BinarySearchTree<T> & scr) = delete;
	BinarySearchTree(BinarySearchTree<T>&& scr) noexcept;
	BinarySearchTree<T>& operator=(const BinarySearchTree <T>& src) = delete;
	BinarySearchTree<T>& operator=(BinarySearchTree <T>&& src) noexcept;
	virtual ~BinarySearchTree();
	bool iterativeSearch(const T& key) const;
	bool insert(const T& key);
	bool deleteKey(const T& key);
	void print(std::ostream& out) const;
	int getCount() const;
	int getHeight() const;
	void iterativeInorderWalk() const;
	void inorderWalk() const;
	void walkByLevels() const;
	bool isSimilar(const BinarySearchTree<T> & other) const;
	bool isIdenticalKey(const BinarySearchTree<T> & other) const;
};

template<class T>
BinarySearchTree<T>::BinarySearchTree()
{
	root_ = nullptr;
}

template<class T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T>&& scr) noexcept
{
	root_(scr.root_);
	{
		scr.root_ = nullptr;
	}
}

template<class T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(BinarySearchTree<T>&& src) noexcept
{
	if (this != &src)
	{
		BinarySearchTree temp(src);
		swap(temp);
	}

	return *this;
}

template<class T>
void BinarySearchTree<T>::swap(BinarySearchTree & other) noexcept
{
	std::swap(root_, other.root_);
}

template<class T>
BinarySearchTree<T>::~BinarySearchTree()
{
	if (root_ == nullptr)
	{
		return;
	}

	std::stack<Node<T>*> nodeStack;
	nodeStack.push(root_);

	while (!nodeStack.empty())
	{
		Node<T>* currentNode = nodeStack.top();
		nodeStack.pop();

		if (currentNode->left_)
		{
			nodeStack.push(currentNode->left_);
		}
		if (currentNode->right_)
		{
			nodeStack.push(currentNode->right_);
		}

		delete currentNode;
	}
}

template<class T>
bool BinarySearchTree<T>::iterativeSearch(const T & key) const
{
	Node<T>* temp = iterativeSearchNode(key);
	return temp != nullptr;
}

template<class T>
BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::iterativeSearchNode(const T & key) const
{
	Node<T>* temp = root_;
	while (temp != nullptr && temp->key_ != key)
	{
		if (key < temp->key_)
		{
			temp = temp->left_;
		}
		else
		{
			temp = temp->right_;
		}
	}
	return temp;
}

template<class T>
bool BinarySearchTree<T>::insert(const T& key)
{
	if (root_ == nullptr)
	{
		root_ = new Node<T>(key);
		return true;
	}

	Node<T>* current_node = root_;
	Node<T>* parent_node = nullptr;
	while (current_node != nullptr)
	{
		parent_node = current_node;
		if (key < current_node->key_)
		{
			current_node = current_node->left_;
		}
		else if (key > current_node->key_)
		{
			current_node = current_node->right_;
		}
		else
		{
			return false;
		}
	}

	Node<T>* new_node = new Node<T>(key);
	if (key < parent_node->key_)
	{
		parent_node->left_ = new_node;
		new_node->p_ = parent_node;
	}
	else
	{
		parent_node->right_ = new_node;
		new_node->p_ = parent_node;
	}
	return true;
}

template<class T>
bool BinarySearchTree<T>::deleteKey(const T& key)
{
	Node<T>* nodeToDelete = iterativeSearchNode(key);
	if (nodeToDelete == nullptr)
	{
		return false;
	}

	if (nodeToDelete->left_ == nullptr && nodeToDelete->right_ == nullptr)
	{
		if (nodeToDelete == root_)
		{
			root_ = nullptr;
		}
		else
		{
			Node<T>* parent = nodeToDelete->p_;
			if (key < parent->key_)
			{
				parent->left_ = nullptr;
			}
			else
			{
				parent->right_ = nullptr;
			}
		}
		delete nodeToDelete;
	}

	else if (nodeToDelete->left_ == nullptr || nodeToDelete->right_ == nullptr)
	{
		Node<T>* child = (nodeToDelete->left_ != nullptr) ? nodeToDelete->left_ : nodeToDelete->right_;
		if (nodeToDelete == root_)
		{
			root_ = child;
		}
		else
		{
			Node<T>* parent = nodeToDelete->p_;
			if (key < parent->key_)
			{
				parent->left_ = child;
				child->p_ = parent;
			}
			else
			{
				parent->right_ = child;
				child->p_ = parent;
			}
		}
		delete nodeToDelete;
	}

	else
	{
		Node<T>* minRightSubtree = nodeToDelete->right_;
		while (minRightSubtree->left_ != nullptr)
		{
			minRightSubtree = minRightSubtree->left_;
		}

		T temp_key = minRightSubtree->key_;
		deleteKey(temp_key);
		nodeToDelete->key_ = temp_key;
	}
	return true;
}

template<class T>
void BinarySearchTree<T>::printNode(Node<T>* node, std::ostream& out) const
{
	if (node != nullptr)
	{
		out << node->key_ << " ";
		if (node->left_ != nullptr || node->right_ != nullptr)
		{
			if (node->left_ != nullptr && node->right_ == nullptr)
			{
				out << "( ";
				printNode(node->left_, out);
				out << " ; _ )";
			}
			else if (node->left_ == nullptr && node->right_ != nullptr)
			{
				out << "( _ ; ";
				printNode(node->right_, out);
				out << " )";
			}
			else
			{
				out << "(";
				printNode(node->left_, out);
				out << " ";
				printNode(node->right_, out);
				out << ")";
			}
		}
	}
}

template<class T>
void BinarySearchTree<T>::print(std::ostream& out) const
{
	printNode(root_, out);
}

template<class T>
int BinarySearchTree<T>::getCount(const Node<T>* node) const
{
	if (node == nullptr) 
	{
		return 0;
	}
	return (1 + getCount(node->left_) + getCount(node->right_));
}

template<class T>
int BinarySearchTree<T>::getCount() const
{
	return getCount(root_);
}

template<class T>
int BinarySearchTree<T>::getHeight(const Node<T>* node) const
{
    if (node == nullptr) 
	{
        return -1;
    }
    int leftHeight = getHeight(node->left_);
    int rightHeight = getHeight(node->right_);
    return 1 + std::max(leftHeight, rightHeight);
}

template<class T>
int BinarySearchTree<T>::getHeight() const
{
	return getHeight(root_);
}

template<class T>
void BinarySearchTree<T>::inorderWalk(Node<T>* node) const
{
	if (node == nullptr)
	{
		return;
	}
	inorderWalk(node->left_);
	std::cout << node->key_ << " ";
	inorderWalk(node->right_);
}

template<class T>
void BinarySearchTree<T>::inorderWalk() const
{
	inorderWalk(root_);
}

template<class T>
void BinarySearchTree<T>::iterativeInorderWalk() const
{
	std::stack<Node<T>*> s;
	Node<T>* curr = root_;
	while (curr != nullptr || !s.empty())
	{
		while (curr != nullptr)
		{
			s.push(curr);
			curr = curr->left_;
		}
		curr = s.top();
		s.pop();
		std::cout << curr->key_ << " ";
		curr = curr->right_;
	}
}

template<class T>
void BinarySearchTree<T>::walkByLevels() const
{
	if (root_ == nullptr)
	{
		return;
	}
	std::queue<Node<T>*> q;
	q.push(root_);
	while (!q.empty())
	{
		Node<T>* curr = q.front();
		q.pop();
		std::cout << curr->key_ << " ";
		if (curr->left_ != nullptr)
		{
			q.push(curr->left_);
		}
		if (curr->right_ != nullptr)
		{
			q.push(curr->right_);
		}
	}
}

template<class T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T>& other) const
{
	std::stack<Node<T>*> current;
	std::stack<Node<T>*> source;
	Node<T>* cur = root_;
	Node<T>* src = other.root_;

	while ((cur != nullptr || !current.empty()) || (src != nullptr || !source.empty()))
	{
		while (true)
		{
	
			if (cur == nullptr && src == nullptr)
			{
				break;
			}
			else if (cur == nullptr && src != nullptr)
			{
				source.push(src);
				src = src->left_;
			}
			else if (cur != nullptr && src == nullptr)
			{
				current.push(cur);
				cur = cur->left_;
			}
			else if (cur != nullptr && src != nullptr)
			{
				current.push(cur);
				source.push(src);
				cur = cur->left_;
				src = src->left_;
			}
		}

		if (current.empty() || source.empty())
		{
			return false;
		}

		cur = current.top();
		current.pop();
		src = source.top();
		source.pop();

		if (cur->key_ == src->key_)
		{
			cur = cur->right_;
			src = src->right_;
		}
		else
		{
			return false;
		}
	}

	if (current.empty() && source.empty())
	{
		return true;
	}
	return false;
}

template<class T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T>& other) const
{
	std::stack<Node<T>*> s;
	Node<T>* curr = root_;
	while (curr != nullptr || !s.empty())
	{
		while (curr != nullptr)
		{
			s.push(curr);
			curr = curr->left_;
		}
		curr = s.top();
		s.pop();
		if (other.iterativeSearch(curr->key_))
		{
			return true;
		}
		curr = curr->right_;
	}
	return false;
}