﻿#include <iostream>
#include "BinarySearchTree.h"

int main()
{
	BinarySearchTree<int> simil1;
	simil1.insert(5);
	simil1.insert(51);
	simil1.insert(30);
	simil1.insert(3);
	simil1.insert(10);

	BinarySearchTree<int> simil2;
	simil2.insert(51);
	simil2.insert(3);
	simil2.insert(5);
	simil2.insert(10);
	simil2.insert(30);

	BinarySearchTree<int> tr;
	tr.insert(9);
	tr.insert(15);
	tr.insert(15);
	tr.insert(10);
	tr.insert(7);
	tr.insert(3);
	tr.insert(20);
	tr.insert(17);
	tr.insert(8);
	tr.insert(5);
	tr.insert(2);

	(tr.iterativeSearch(20)) ? std::cout << "20 in tree 'tr' \n" : std::cout << "20 not in tree 'tr'\n";
	(tr.iterativeSearch(55)) ? std::cout << "55 in tree 'tr' \n" : std::cout << "55 not in tree 'tr'\n";
	std::cout << "Structure of 'tr': ";
	tr.print(std::cout);
	std::cout << "\nIterative inorder walk for 'tr': ";
	tr.iterativeInorderWalk();

	std::cout << "\nRecursion inorder walk for 'tr': ";
	tr.inorderWalk();
	
	std::cout << "\nWalk by levels in 'tr': ";
	tr.walkByLevels();

	std::cout << "\nCount of nodes of 'tr': " << tr.getCount();
	std::cout << "\nHeight of 'tr': " << tr.getHeight();

	tr.deleteKey(9);
	tr.deleteKey(20);
	tr.deleteKey(5);
	std::cout << "\nStructure of 'tr' after delete nodes: ";
	tr.print(std::cout);
	std::cout << "\nIterative inorder walk for 'tr' after delete nodes: ";
	tr.iterativeInorderWalk();
	std::cout << "\n\n";

	simil1.isSimilar(simil2) ? std::cout << "Trees 'simil1' and 'simil2' are similar \n" : std::cout << "Trees 'simil1' and 'simil2' are not similar\n";
	simil1.isSimilar(tr) ? std::cout << "\nTrees 'simil1' and 'tr' are similar \n" : std::cout << "\nTrees 'simil1' and 'tr' are not similar\n";
	simil1.isIdenticalKey(tr) ? std::cout << "\nTrees 'simil1' and 'tr' have the same keys\n" : std::cout << "\nTrees 'simil1' and 'tr' not have the same keys\n";

	return 0;
}